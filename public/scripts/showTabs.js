
let currentTab;

function setUpEventHandlers(){
    const pedidostab=document.querySelector("#draftTab");
    const pedidotab=document.querySelector("#requestTab");
    const nuevostab=document.querySelector("#finishedTab");
    pedidostab.addEventListener('click', putFocusOnTab);
    pedidotab.addEventListener('click', putFocusOnTab);
    nuevostab.addEventListener('click', putFocusOnTab);
}

function putFocusOnTab(event){
    let buttonId=event.currentTarget.id;
    let tabId=buttonId.split('T')[0];
    showTab(tabId);
}

function hideCurrentTab(){
    if(currentTab!==undefined){
        currentTab.classList.add("hidden");
    }
}

function showTab(tabId){
    hideCurrentTab();
    const tab=document.querySelector("#"+tabId);
    tab.classList.remove("hidden");
    currentTab=tab;
}


/*function tabsToOpen(){
    const tab=document.querySelector("."+tabId+"Tab");
    if(tab==="nuevoTab"){
        tab.addEventListener('click', showTab("draft"));
    }
    else if(tab==="pedidoTab"){
        tab.addEventListener('click', showTab("request"));
    }else if(tab==="pedidosTab"){
        tab.addEventListener('click', showTab("finished")); 
    }
}*/