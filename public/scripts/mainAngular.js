const URL="https://sisweb-product-orders.herokuapp.com";
const DRAFT_STATUS="draft";
const REQUESTED_STATUS="requested";
const FINISHED_STATUS="finished";
const ORDER_URL="/order";
const PRODUCTS_URL="/products";
const ORDERS_BY_STATUS_URL="/orders?status=";

const productObj={};
let currentOrder;
let userID="5271024";


let app = angular.module("myApp", ["ngRoute"]);
app.config(function($routeProvider) {
    $routeProvider
    .when("/newOrder", {
        templateUrl : "/newOrder.html",
        controller : "newController"
    })
    .when("/", {
        templateUrl : "/tabsOrders.html",
        controller : "tabsController"
    })
    .when("/viewOrder/:id", {
        templateUrl : "/newOrder.html",
        controller : "viewOrderController"
    })
});

app.controller("tabsController", function($scope, $http){/*scope==model, http se comunica con el servidor con la info */
    let req = {
        method: 'GET',
        url: URL+ORDERS_BY_STATUS_URL+DRAFT_STATUS,
        headers: {
          User:userID
        }
    }
    fetchOrders($http, req, $scope);
    showTab("draft");


    $scope.deleteOrder = (x, statusTab) => {

        
     switch(statusTab) {
        case 'draft':
        deleteOrderBD($http, $scope.draftOrders[x]._id);
        $scope.draftOrders.splice(x, 1); 
            break;
        case 'request':
        deleteOrderBD($http, $scope.requestedOrders[x]._id);
        $scope.requestedOrders.splice(x, 1); 
            break;
        case 'finished':
        deleteOrderBD($http, $scope.finishedOrders[x]._id);
        $scope.finishedOrders.splice(x, 1); 
            break;
            }
    }

});

function fetchOrders($http, req, $scope){
    fetchDraftOrders($http,req,$scope);
    req = {
        method: 'GET',
        url: URL+ORDERS_BY_STATUS_URL+REQUESTED_STATUS,
        headers: {
          User:userID
        }
    }
    fetchRequestOrders($http,req,$scope);
    req = {
        method: 'GET',
        url: URL+ORDERS_BY_STATUS_URL+FINISHED_STATUS,
        headers: {
          User:userID
        }
    }
    fetchFinishedOrders($http,req,$scope);
}



function deleteOrderBD(httpService, id){
    
    let req = {
        method: 'DELETE', 
        url: URL+ORDER_URL+"/"+id,
        data : JSON.stringify(currentOrder),
        headers: {
          User:userID
        }
    };

    httpService(req)
        .then((response)=>{
            window.location.replace('/');//redirect
        })
        .catch((error)=>{
            console.log("Error: "+error);
        });
}



//GET ORDERS BY STATUS


function fetchDraftOrders(httpService, req, scope){
    httpService(req)
    .then((response)=>{
        scope.draftOrders=response.data;
    })
    .catch((error)=>{
        console.log("Error: "+error);
    });
}

function fetchRequestOrders(httpService, req, scope){
    httpService(req)
    .then((response)=>{
        scope.requestedOrders=response.data;
    })
    .catch((error)=>{
        console.log("Error: "+error);
    });
}

function fetchFinishedOrders(httpService, req, scope){
    httpService(req)
    .then((response)=>{
        scope.finishedOrders=response.data;
    })
    .catch((error)=>{
        console.log("Error: "+error);
    });
}


app.controller("newController", function ($scope, $http){
    let req = {
        method: 'GET',
        url: URL+PRODUCTS_URL,
        headers: {
          User:userID
        }
    };
    fetchProduct($http, req, $scope);
    $scope.order={
        "customer": "", 
        "products": [], 
        "status": DRAFT_STATUS
    };/*recibir de la url */

    processLeadsToSave($http, $scope);
});



function fetchProduct(httpService, req, scope){
    httpService(req)
    .then((response)=>{
        scope.products=response.data;
        putInObjectProducts(scope.products);
        scope.productObj=productObj;
        
    })
    .catch((error)=>{
        console.log("Error: "+error);
    });
}


function putInObjectProducts(array){
    for(product of array){
        productObj[product._id]=product.name;
    }
}

app.controller("viewOrderController", function($scope, $http, $routeParams){
    let id=$routeParams.id;
    let req = {
        method: 'GET',
        url: URL+ORDER_URL+"/"+id,
        headers: {
          User:userID
        }
    };

    getOrder($http, req, $scope);

    req = {
        method: 'GET',
        url: URL+PRODUCTS_URL,
        headers: {
          User:userID
        }
    };
    fetchProduct($http, req, $scope);
    processLeadsToSave($http, $scope);

});


function getOrder(httpService, req, scope){
    httpService(req)
    .then((response) => {
        scope.order = response.data;
        currentOrder = response.data;
    })
    .catch((error) => {
        console.log("Error: "+error);
    });
}


function processLeadsToSave($http, $scope){
  

    $scope.saveAsRequest = () => {
        currentOrder.status = REQUESTED_STATUS;
        saveOrder($http);
    }

    $scope.save = () => {

        saveOrder($http);
    }

    $scope.addProduct = () => {
        let select = document.querySelector("#table-select");
        let id=select.value;
        $scope.order.products.push(id);
        currentOrder=$scope.order;
        increaseRowCounter();
        console.log(currentOrder);
    }

    $scope.deleteProduct = (x) => { 
        decreaseRowCounter();   
        $scope.order.products.splice(x, 1);
        
    }
}


function saveOrder(httpService){
    let req;
    if(currentOrder._id){ /*PUT=PARA ACTUALIZAR */
        let id=currentOrder._id;
        updateOrder();
        req = {
            method: 'PUT', 
            url: URL+ORDER_URL+"/"+id,
            data : JSON.stringify(currentOrder),
            headers: {
              User:userID
            }
        };
    }
    else{
        req = {
            method: 'POST',
            url: URL+ORDER_URL,
            data : JSON.stringify(currentOrder),
            headers: {
              User:userID
            }
        };
    }
    httpService(req)
        .then((response)=>{
            window.location.replace('/');//redirect.- cuando hace lick en guardar ira a tabs
        })
        .catch((error)=>{
            console.log("error: "+error);
        });
}



function updateOrder(){
    let auxiliarOrder=currentOrder;
    currentOrder={
            "customer": "", 
            "products": [], 
            "status": DRAFT_STATUS
        };
    currentOrder.customer=auxiliarOrder.customer;
    currentOrder.products=auxiliarOrder.products;
    currentOrder.status=auxiliarOrder.status;
}



