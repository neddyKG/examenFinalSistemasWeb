const express=require('express');
const app=express();


app.set('view engine', 'html');
app.use(express.static("public"));

async function startServer() {
    app.listen(3000);
    console.log("Server is up and running.\nOpen http://localhost:3000 to start");
}

startServer();